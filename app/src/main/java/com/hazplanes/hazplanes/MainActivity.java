package com.hazplanes.hazplanes;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;


public class MainActivity extends ActionBarActivity {

    private AdView mAdView;
    public static boolean mostrarPubli = false;
    public static String urlapiversion = "v2/";

    public Location location;
    public static double clong = 0;
    public static double clat = 0;

    private static final String PROPERTY_ID = "UA-48359390-3";
    private Tracker tracker;
    HashMap<AnalyticsApp.TrackerName, Tracker> mTrackers = new HashMap<AnalyticsApp.TrackerName, Tracker>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GoogleAnalytics.getInstance(this).newTracker(PROPERTY_ID);
        GoogleAnalytics.getInstance(this).getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
        tracker = getTracker(AnalyticsApp.TrackerName.APP_TRACKER);
        tracker.setScreenName("MainActivity");
        tracker.send(new HitBuilders.AppViewBuilder().build());

        setContentView(R.layout.activity_main);

        if(mostrarPubli) {
            mAdView = new AdView(this);
            mAdView.setAdUnitId("ca-app-pub-0179166557701407/2766495740");
            mAdView.setAdSize(com.google.android.gms.ads.AdSize.BANNER);

            FrameLayout layout = (FrameLayout) findViewById(R.id.container);

            layout.addView(mAdView);

            /// Now whenever I want to position it on the bottom I do this:
            mAdView.setLayoutParams(new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM));

            mAdView.loadAd(new AdRequest.Builder().build());
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new HazplanesFragment(), "Frag")
                    .commit();
        }

        //placing location
        LocationManager loc = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        location = loc.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location locat) {
                // Called when a new location is found by the network location provider.
                //makeUseOfNewLocation(location);
                location = locat;
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        loc.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        if (location != null) {
            clong = location.getLongitude();
            clat = location.getLatitude();    //current longitude and latitude
        } else {
            loc.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            location = loc.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                clong = location.getLongitude();
                clat = location.getLatitude();    //current longitude and latitude
            }
        }
        // Remove the listener you previously added
        loc.removeUpdates(locationListener);
    }


    synchronized Tracker getTracker(AnalyticsApp.TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == AnalyticsApp.TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (trackerId == AnalyticsApp.TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts & uncaught exceptions etc.
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //Stop the analytics tracking
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /** Called before the activity is destroyed. */
    @Override
    public void onDestroy() {
        // Destroy the AdView.
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

}
