package com.hazplanes.hazplanes;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class HazplanesFragment extends Fragment {

    private static CustomListAdapter mHazplanesAdapter;
    private Integer pos = 0;

    public HazplanesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Adapter
        SpinnerAdapter adapter =
                ArrayAdapter.createFromResource(getActivity().getActionBar().getThemedContext(), R.array.actions,
                        android.R.layout.simple_spinner_dropdown_item);
        // Callback
        ActionBar.OnNavigationListener callback = new ActionBar.OnNavigationListener() {

            @Override
            public boolean onNavigationItemSelected(int position, long id) {
                // Do stuff when navigation item is selected
                //Log.e("NavigationItemSelected", items[position]); // Debug
                pos = position;
                updatePlanes("");
                mHazplanesAdapter.notifyDataSetChanged();
                return true;
            }

        };

        // Action Bar
        ActionBar actions = getActivity().getActionBar();
        actions.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actions.setDisplayShowTitleEnabled(false);
        actions.setListNavigationCallbacks(adapter, callback);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.hazplanesfragment, menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ArrayList<EventoItem> image_details = new ArrayList<EventoItem>();

        mHazplanesAdapter = new CustomListAdapter(getActivity(), image_details);

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (50 * scale + 0.5f);
        int dpAsPixels16 = 0;//(int) (16 * scale + 0.5f);
        if(MainActivity.mostrarPubli) {
            rootView.setPadding(dpAsPixels16, 0, dpAsPixels16, dpAsPixels);
        } else {
            rootView.setPadding(dpAsPixels16, 0, dpAsPixels16, dpAsPixels16);
        }

        final ListView listView = (ListView) rootView.findViewById(R.id.listview_planes);
        listView.setAdapter(mHazplanesAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            TextView tx = (TextView) view.findViewById(R.id.list_item_id);
            if(tx != null) {
                String s = tx.getText().toString();
                Intent intent = new Intent(getActivity(), DetailActivity.class).putExtra(Intent.EXTRA_TEXT, s);
                startActivity(intent);
            }
            }
        });

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_refresh) {
            updatePlanes("");
            return true;
        } else if(id == R.id.action_datepickup) {
            Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                    datePickerListener, mYear, mMonth, mDay);
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String v = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            updatePlanes(v);
        }
    };

    public void updatePlanes(String date) {
        FetchPlanesTask planesTask = new FetchPlanesTask();
        planesTask.execute(date);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mHazplanesAdapter.isEmpty()) {
            updatePlanes("");
        }
    }

    public class FetchPlanesTask extends AsyncTask<String, Void, ArrayList<EventoItem>> {

        private ProgressDialog pDialog;
        private final String LOG_TAG = FetchPlanesTask.class.getSimpleName();

        ListView listView = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Cogiendo datos ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
            listView = (ListView) getActivity().findViewById(R.id.listview_planes);
        }

        protected ArrayList<EventoItem> doInBackground(String... params) {
            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String hazplanesJsonStr;
            ArrayList<EventoItem> result = null;

            String fecha = params[0];
            String fin, ini;
            String filter;

            try {

                URL url = new URL("http://hazplanes.com/api/" + MainActivity.urlapiversion);

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);

                if(pos < 1 || pos > 7) {
                    Date d = new Date();
                    if (fecha.isEmpty()) {
                        String hora = new SimpleDateFormat("H").format(d);
                        // Get today as a Calendar
                        Calendar today = Calendar.getInstance();
                        // Subtract 1 day
                        today.add(Calendar.DATE, -1);
                        if(Integer.parseInt(hora) < 6) {
                            ini = today.toString() + " " + new SimpleDateFormat("0:mm:ss").format(d);
                            fin = today.toString() + " 05:59:59";
                            filter = "fecha between '" + ini + "' and '" + fin + "' and publicado = 1";
                        } else {
                            ini = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
                            fin = new SimpleDateFormat("yyyy-MM-dd 23:59:59").format(d);
                            filter = "(((fecha between '"+ ini + "' and '" + fin + "') or (date(fecha) = date('" + ini + "') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1";
                        }
                    } else {
                        Date d2 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
                        ini = new SimpleDateFormat("yyyy-MM-dd 00:00:00").format(d2);
                        fin = new SimpleDateFormat("yyyy-MM-dd 23:59:59").format(d2);
                        filter = "(((fecha between '"+ ini + "' and '" + fin + "') or (date(fecha) = date('" + ini + "') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1";
                    }
                    if(pos == 8) {
                        String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
                        ini = new SimpleDateFormat("yyyy-MM-dd 06:00:00").format(d);
                        filter = "fecha between '" + ini + "' and '" + now + "' and publicado = 1";
                        nameValuePairs.add(new BasicNameValuePair("order", "time(fecha) desc, nombre"));
                    }
                    nameValuePairs.add(new BasicNameValuePair("controller", "GeneralController"));
                } else {
                    if (fecha.isEmpty()) {
                        String fin2, ini2;
                        Date d = new Date();

                        ini = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
                        // Get today as a Calendar
                        Calendar today = Calendar.getInstance();
                        // Subtract 1 day
                        today.add(Calendar.DATE, -1);
                        ini2 = today.toString() + " " + new SimpleDateFormat("HH:mm:ss").format(d);
                        fin2 = today.toString() + " 05:59:59";

                        filter = "(fecha >= '" + ini + "' or (date(fecha) = date('" + ini + "') and (time(fecha) < '06:00:00' or todoeldia = 1)) or fecha between '" + ini2 + "' and '" + fin2 + "') and publicado = 1";
                    } else {
                        // Get today as a Calendar
                        Date d = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
                        ini = new SimpleDateFormat("yyyy-MM-dd 00:00:00").format(d);
                        fin = new SimpleDateFormat("yyyy-MM-dd 23:59:59").format(d);
                        filter = "(fecha between '"+ ini + "' and '" + fin + "') and publicado = 1";
                    }
                    if(pos == 1) {
                        nameValuePairs.add(new BasicNameValuePair("controller", "Peliculas"));
                    } else if(pos == 2) {
                        nameValuePairs.add(new BasicNameValuePair("controller", "Conciertos"));
                    } else if(pos == 3) {
                        nameValuePairs.add(new BasicNameValuePair("controller", "Obras"));
                    } else if(pos == 4) {
                        nameValuePairs.add(new BasicNameValuePair("controller", "Deportes"));
                    } else if(pos == 5) {
                        nameValuePairs.add(new BasicNameValuePair("controller", "Exposiciones"));
                        if (fecha.isEmpty()) {
                            filter = "((inicio <= CURDATE() and fin >= CURDATE()) or permanente = 1) and publicado = 1";
                        } else {
                            ini = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy-MM-dd").parse(fecha));
                            filter = "((inicio <= '" + ini + "' and fin >= '" + ini + "') or permanente = 1) and publicado = 1";
                        }
                    } else if(pos == 6) {
                        nameValuePairs.add(new BasicNameValuePair("controller", "Cursos"));
                    } else if(pos == 7) {
                        nameValuePairs.add(new BasicNameValuePair("controller", "Eventos"));
                    }
                }
                nameValuePairs.add(new BasicNameValuePair("action", "Read"));
                nameValuePairs.add(new BasicNameValuePair("filter", filter));
                if(pos < 1 || pos > 7) {
                    nameValuePairs.add(new BasicNameValuePair("agendaMap", "1"));
                } else {
                    nameValuePairs.add(new BasicNameValuePair("agenda", "1"));
                }

                StringBuilder postData = new StringBuilder();
                for (NameValuePair param : nameValuePairs) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getName(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                byte[] postDataBytes = postData.toString().getBytes("UTF-8");

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                urlConnection.setDoOutput(true);
                urlConnection.getOutputStream().write(postDataBytes);

                //urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuilder buffer = new StringBuilder();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    line += "\n";
                    buffer.append(line);
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                hazplanesJsonStr = buffer.toString();

                JSONParser parser = new JSONParser();
                try {
                    result = parser.getHazPlanesDataFromJson(hazplanesJsonStr);
                } catch(JSONException e) {
                    Log.e(LOG_TAG, e.getMessage(), e);
                    e.printStackTrace();
                    return null;
                }
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attemping
                // to parse it.
                return null;
            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<EventoItem> result) {
            pDialog.dismiss();
            if(result != null) {
                TextView t = (TextView) getActivity().findViewById(R.id.noevents);
                if(result.isEmpty()) {
                    t.setText(getString(R.string.noEvents));
                } else {
                    t.setText("");
                }
                mHazplanesAdapter = new CustomListAdapter(getActivity(), result);
                listView.setAdapter(mHazplanesAdapter);
            }
        }
    }
}
