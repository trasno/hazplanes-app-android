package com.hazplanes.hazplanes;

import org.json.JSONArray;

import java.util.ArrayList;

public class EventoItem {

    private String idEvento = "";
    private String nombre = "";
    private String genero = "";
    private String fecha = "";
    private String tipoevento = "";
    private String posterURL = "";
    private String sinopsis = "";
    private String notas = "";
    private String menciones = "";
    private String web = "";
    private String creado = "";
    private String actualizado = "";
    private Boolean agotado = false;
    private Boolean cancelado = false;
    private Boolean infantil = false;
    private Boolean reserva = false;
    private Boolean todoeldia = false;
    private String limitereserva = "";
    private String video = "";
    private double coordlat = 0;
    private double coordlong = 0;

    //Campos si es pelicula
    private String director = "";
    private String actores = "";
    private String nacionalidad = "";
    private String distribuidora = "";
    private String edad = "";
    private String duracion = "";
    private String fechaestreno = "";

    private ArrayList<JSONArray> sesiones;

    public String getPosterUrl() {
        return posterURL;
    }

    public void setPosterUrl(String url) {
        this.posterURL = "http://hazplanes.com/" + url;
    }

    public String getID() {
        return idEvento;
    }

    public void setID(String id) {
        this.idEvento = id;
    }

    public String getNombre() { return nombre; }

    public void setNombre(String name) {
        this.nombre = name;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genre) {
        this.genero = genre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String date) {
        this.fecha = date;
    }

    public String getTipoevento() {
        return tipoevento;
    }

    public void setTipoevento(String type) {
        this.tipoevento = type;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getMenciones() {
        return menciones;
    }

    public void setMenciones(String menciones) {
        this.menciones = menciones;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getActualizado() {
        return actualizado;
    }

    public void setActualizado(String actualizado) {
        this.actualizado = actualizado;
    }

    public String getCreado() {
        return creado;
    }

    public void setCreado(String creado) {
        this.creado = creado;
    }

    public Boolean getAgotado() {
        return agotado;
    }

    public void setAgotado(Boolean agotado) {
        this.agotado = agotado;
    }

    public Boolean getCancelado() {
        return cancelado;
    }

    public void setCancelado(Boolean cancelado) {
        this.cancelado = cancelado;
    }

    public Boolean getInfantil() {
        return infantil;
    }

    public void setInfantil(Boolean infantil) {
        this.infantil = infantil;
    }

    public Boolean getReserva() {
        return reserva;
    }

    public void setReserva(Boolean reserva) {
        this.reserva = reserva;
    }

    public Boolean getTodoeldia() {
        return todoeldia;
    }

    public void setTodoeldia(Boolean todoeldia) {
        this.todoeldia = todoeldia;
    }

    public String getLimitereserva() {
        return limitereserva;
    }

    public void setLimitereserva(String limitereserva) {
        this.limitereserva = limitereserva;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public double[] getCoordenadas() {
        double[] coord = new double[2];
        coord[0] = coordlat;
        coord[1] = coordlong;

        return coord;
    }

    public void setCoordenadas(String coordenadas) {
        coordenadas = coordenadas.replace("(","");
        coordenadas = coordenadas.replace(")","");
        coordenadas = coordenadas.replace(" ","");
        this.coordlat = Double.parseDouble(coordenadas.split(",")[0]);
        this.coordlong = Double.parseDouble(coordenadas.split(",")[1]);
    }

    //Peliculas
    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActores() {
        return actores;
    }

    public void setActores(String actores) {
        this.actores = actores;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getDistribuidora() {
        return distribuidora;
    }

    public void setDistribuidora(String distribuidora) {
        this.distribuidora = distribuidora;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion + " min.";
    }

    public String getFechaestreno() {
        return fechaestreno;
    }

    public void setFechaestreno(String fechaestreno) {
        this.fechaestreno = fechaestreno;
    }

    public ArrayList<JSONArray> getSesiones() { return sesiones; }

    public void setSesiones(ArrayList<JSONArray> sesiones) { this.sesiones = sesiones; }

    @Override
    public String toString() {
        return "[ nombre=" + nombre + ", genero=" + genero + " , fecha=" + fecha + " , tipoevento=" + tipoevento + "]";
    }
}
